#!/bin/bash

# Remember to run this script using the command "source ./filename.sh"

# Search these processes for the session variable 
# (they are run as the current user and have the DBUS session variable set)
compatiblePrograms=( spotify )

# Attempt to get a program pid
for index in ${compatiblePrograms[@]}; do
    PID=$(pidof -s ${index})
      if [[ "${PID}" != "" ]]; then
            break
      fi
done
  if [[ "${PID}" == "" ]]; then
    echo "Could not detect active login session"
      exit 0
  fi

  QUERY_ENVIRON="$(tr '\0' '\n' < /proc/${PID}/environ | grep "DBUS_SESSION_BUS_ADDRESS" | cut -d "=" -f 2-)"
  if [[ "${QUERY_ENVIRON}" != "" ]]; then
    DBUS_SESSION_BUS_ADDRESS="${QUERY_ENVIRON}"
    echo "DBUS_SESSION_BUS_ADDRESS=${DBUS_SESSION_BUS_ADDRESS}"
   else
     return 1
  fi

