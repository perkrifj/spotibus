#!/bin/bash

#This song will play a song, if one is selected in the Spotify window. I.e. if a song has previously been paused,
#issuing this command will make it play again
function play {
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play
}

#This function will pause a song that is playing
function pause {
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause
}

#This function will play the next song (if there is one in the list) if no song is currently playing
#If there is a song playing, it will pause it
function playpause {
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
}

#This function will start playing the infamous song 'Never Gonna Give You Up' by Rick Astley.
#Use it wisely and at your own discretion.
function rickroll {
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.OpenUri string:spotify:track:6JEK0CvvjDjjMUBFoXShNZ
}

#This function takes a Spotify URI as parameter. 
#The URI can be a song, spotify:track:xXxXxXxXx
#An albun, spotify:album:xXx
#A playlist spotify:user:'user':playlist:XxXxxXXxxXX
function playsong {
if [[ -z $1 ]]; then
  echo "Empty argument. You should pass a valid Spotify 'song-hash', i.e. 6JEK0CvvjDjjMUBFoXShNZ"
else
  dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.OpenUri string:$1
fi
}

#Skips to the next song in the playlist
function next {
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next
}
#Restarts the current song or skips to the previuos
function previous {
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous
}

function help {

printf "Remember to run the program as source, i.e 'source ./spotibus <arg>' \n \n"
printf "Available commands \n"
printf "play : will play a song that is paused \n"
printf "pause : will pause the currently playing song \n"
printf "playpause : if a song is playing it will be paused. If a song is paused it will begin playing \n"
printf "next : will skip to the next song in the playlist \n"
printf "previous : play previous song (NB! Will restart the current song if some time has elapsed. Try issuing the command two times \n"
printf "playsong <arg> : play a song, playlist, or album given as an argument. Valid arguments are spotify URIs, i.e. spotify:track:5oHHMDcVOmPSFrCgdbHPdb \n"

printf "\n \n \n"

printf "perkrifj 2013"

printf "\n\n\n"
return
}

DBUS=$(./dbus-script.sh | awk -F'SS=' '{print $2}')

if ! [[ "${#DBUS}" > 0 ]] ; then
  echo "Failed to set the DBUS variable, verify that Spotify is running!"
  return
fi
export DBUS_SESSION_BUS_ADDRESS=$DBUS

case $1 in
  play)
    echo "running command play" 
    play
    ;;
  pause)
    echo "running command pause"
      pause
    ;;
  playpause)
    playpause
    ;;
  next)
    next
    ;;
  previous)
    previous
    ;;
  playsong)
    playsong $2
    ;;
  rickroll)
    rickroll
    ;;
  help)
    help
    ;;
  --help)
    help
    ;;
esac
